# KopSoftSCADA数据采集与监控,电子看板,Andon安灯系统

****
* KopSoft标签(条码)打印软件 http://print.kopsoft.cn/
* 码云 https://gitee.com/william1984/KopSoftPrint
* GitHub https://github.com/1984william/KopSoftPrint
*
* KopSoft仓库管理系统 http://wms.kopsoft.cn/
* 码云 https://gitee.com/yulou/KopSoftWms
* GitHub https://github.com/lysilver/KopSoftWms
*
* KopSoft制造执行系统 http://mes.kopsoft.cn/
* 码云 https://gitee.com/yulou/KopSoftMES
* GitHub https://github.com/lysilver/KopSoftMES
*
* KopSoftSCADA数据采集与监控,电子看板,Andon安灯系统 http://kanban.kopsoft.cn/
* 码云 https://gitee.com/william1984/KopSoftSCADA
* GitHub https://github.com/1984william/KopSoftSCADA
****
